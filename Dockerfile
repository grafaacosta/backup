FROM oracle/graalvm-ce:19.3.2-java11

RUN gu install native-image

WORKDIR /usr/src/app

RUN curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein --output lein

RUN chmod u+x lein
RUN ./lein

COPY . .

RUN ./lein deps
RUN ./lein do clean, uberjar

RUN ./lein native