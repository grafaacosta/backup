# backup

[![pipeline status](https://gitlab.com/grafaacosta/backup/badges/master/pipeline.svg)](https://gitlab.com/grafaacosta/backup/-/commits/master)

Demo

Useful to loop through a range of folders, delete and zip old files.

Built with chanels and flushing so it can handle as many files as you like. So far the larges run was 23.000 files of aprox 57 KB each into one zip file.

Native compiled with graalvm.

## Installation

[Linux Native v1.0.0](https://gitlab.com/grafaacosta/backup/uploads/fe6ba527e843c2ae1863ccb13a1d59c7/backup.zip)

[Uberjar v1.0.0](https://gitlab.com/grafaacosta/backup/uploads/2e64f4c47960728dd6941845f229129d/backup-standalone.jar.zip)

## Usage

```bash
$ ./backup [params]
```

## Options

  - --ini-folder: number (default 0)
  - --end-folder: number (default 100) 
  - --base-path: string (path "$user.dir")
  - --dest-path: string (path "$user.dir/dest")
  - --max-date: number (default 2020) 

## Examples

Will loop in base path from folders `000200` to `003000`. Extract the files with a date less than 2021. Zip in 1 dest file and delete the old files.

```bash
$ ./backup --ini-folder 200 \
           --end-folder 3000 \
           --base-path "../reports" \
           --dest-path "../reports/dest" \
           --max-date 2021 \
```

## License

Copyright © 2020 Gonzalo Rafael Acosta

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
