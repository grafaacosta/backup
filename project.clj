(defproject backup "1.0.0"
  :description "Zip and upload files to the cloud through ssh"
  :url "http://gitlab.com/grafaacosta/backup"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "1.1.587"]
                 [com.outpace/config "0.13.2"]
                 [nrepl "0.6.0"]]
  :target-path "target/%s"
  :main backup.core
  :profiles {
             :uberjar {:global-vars {*warn-on-reflection* true}
                       :aot :all
                       :jar-name "backup.jar"
                       :uberjar-name "backup-standalone.jar"}
             :dev {:plugins [[lein-shell "0.5.0"]]}}
  :aliases {"config" ["run" "-m" "outpace.config.generate"]
            "native"
            ["shell"
             "native-image"
             "--initialize-at-build-time"
             "--enable-all-security-services"
             "--report-unsupported-elements-at-runtime"
             "-H:+ReportExceptionStackTraces"
             "-H:+TraceClassInitialization"
             "-jar" "./target/uberjar/backup-standalone.jar"
             "-H:Name=./target/${:name}"]})
