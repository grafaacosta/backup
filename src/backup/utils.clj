(ns backup.utils
  (:require [clojure.core.async :as async])
  (:import (java.util.zip ZipOutputStream ZipEntry)))

(defmacro with-entry
  [zip entry-name & body]
  `(let [^ZipOutputStream zip# ~zip]
     (.putNextEntry zip# (ZipEntry. ^String ~entry-name))
     ~@body
     (flush)
     (.closeEntry zip#)))

(defn pipe-transduce
  ([xf in] (pipe-transduce 1 xf in))
  ([buf xf in]
   (let [out (async/chan buf xf)]
     (async/go-loop [v (async/<! in)]
       (if (nil? v)
         (async/close! out)
         (do
           (async/>! out v)
           (recur (async/<! in)))))
     out)))
