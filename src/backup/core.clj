(ns backup.core
  (:require [clojure.core.async :as async]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [backup.utils :as utils])
  (:import (java.time.format DateTimeFormatter)
           (java.util.zip ZipOutputStream)
           (java.util UUID)
           (java.lang Integer)
           (java.time LocalDate))
  (:gen-class))

(def FLUSH-COUNT 500)

(defn create-zip [path]
  (ZipOutputStream. (io/output-stream path)))

(defn zip-process [dest-path in]
  (let [out (async/chan 20)
        zip ^ZipOutputStream (create-zip (str dest-path (UUID/randomUUID) ".zip"))]
    (async/thread
      (loop [{filename :filename path :path id :id :as payload} (async/<!! in) count 0]
        (when (= count FLUSH-COUNT)
          (.flush zip)
          (println "info: flushed"))
        (cond
          (nil? filename) (do
                            (.close zip)
                            (async/close! out))
          :else           (when (.exists (io/file path))
                            (if
                             (try
                               (utils/with-entry zip (str id "/" filename)
                                 (with-open [file (io/input-stream path)]
                                   (io/copy file zip)))
                               (async/>!! out payload)
                               true
                               (catch Exception e
                                 (println "error: " (.getMessage e))
                                 false))
                              (recur (async/<!! in)
                                     (if (< count FLUSH-COUNT) (inc count) 0))
                              (recur nil 0))))))
    out))

;;;;;;; folder
(defn pad [client] (format "%06d" client))

(defn is-folder? [base-path]
  (comp (map (fn [client-number]
               (str base-path (pad client-number) "/")))
        (filter (fn [path]
                  (let [is-directory (.isDirectory (io/file path))]
                    (println "info: is directory " path is-directory)
                    is-directory)))))

(defn get-folders [base-path ini end]
  (let [out (async/chan 1 (is-folder? base-path))]
    (println "info: range " ini " " end)
    (async/onto-chan out (range ini end))
    out))

(def date-formatter (DateTimeFormatter/ofPattern "yyyy-MM-d"))

(defn str-to-date [str]
  (let [[_ & re] (re-find #"^([0-9]{4})([0-9]{2})([0-9]{2})" str)
        date (str/join "-" re)]
    (LocalDate/parse date date-formatter)))

(defn get-file-params [{filename :filename :as file}]
  (let [re (re-find #"([0-9]{8})([0-9]{6})[0-9]{2}.doc$" filename)]
    (merge file
           {:id       (get re 2 "no-client")
            :date     (str-to-date (get re 1 "20200101"))})))

(defn filter-year [year]
  (comp (map get-file-params)
        (filter (comp (partial re-matches #"^[a-zA-Z0-9_]+\.doc$")
                      :filename))
        (filter (fn [{date :date}]
                  (< (.getYear ^LocalDate date) year)))))

(defn delete-files [in]
  (loop [{path :path id :id} (async/<!! in) last-id nil]
    (if-not (nil? path)
      (do
        (println "info: removing file " path)
        (io/delete-file path)
        (recur (async/<!! in) id))
      last-id)))

(defn get-files [in]
  (let [out (async/chan)]
    (async/thread
      (loop [path (async/<!! in)]
        (if-not (nil? path)
          (let [folder (io/file path)]
            (println "info: opening folder " path)
            (doseq [file (.list folder)]
              (async/>!! out
                         {:filename file
                          :path     (str path file)}))
            (recur (async/<!! in)))
          (async/close! out))))
    out))

(defn run-module [ini end base-path dest-path year]
  (let [folders (get-folders base-path ini end)
        files (get-files folders)
        delete (zip-process dest-path
                            (utils/pipe-transduce (filter-year year) files))]
    (loop [i 4 buf []]
      (if (> i 0)
        (recur (dec i) (conj buf (async/thread (delete-files delete))))
        (loop [[t & rest] buf last (Integer. "0")]
          (if-not (nil? t)
            (if-let [next (async/<!! t)]
              (recur rest (max last (Integer. ^String next)))
              (recur rest last))
            (println "info: last folder " last)))))))

(defn index-of [val arr]
  (get (first (filter #(= (second %) val)
                      (map-indexed vector arr))) 0))

(defn get-param [args param]
  (when-let [index (index-of param args)]
    (get args (inc index))))

(defn get-params [params defaults args]
  (merge defaults
         (zipmap (keys params)
                 (filter (comp not nil?)
                         (map (partial get-param args) (vals params))))))

(def required-params
  {:ini       "--folder-ini"
   :end       "--folder-end"
   :base-path "--base-path"
   :dest-path "--dest-path"
   :max-date  "--max-date"})

(defn get-canonical-path [path]
  (str (.getCanonicalPath (io/file path)) "/"))

(defn -main [& args]
  (let [defaults {:ini "0"
                  :end "100"
                  :base-path (System/getProperty "user.dir")
                  :dest-path (str (System/getProperty "user.dir") "/dest")
                  :max-date "2020"}
        params (get-params required-params defaults (vec args))]
    (run-module (Integer. ^String (get params :ini))
                (Integer. ^String (get params :end))
                (get-canonical-path
                 (get params :base-path))
                (get-canonical-path
                 (get params :dest-path))
                (Integer. ^String (get params :max-date)))))
